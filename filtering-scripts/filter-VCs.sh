exit_if_error(){
    if [ "$?" -ne "0" ]; then exit 1; fi
}

echo "usage: ./filter-bench.sh <target-bench> <to-remove>"
echo "     with"
echo "       <target-bench> in {C-VCS-ALL, SPARK-VCS-ALL}"
echo "       <to-remove> a file containing the list of VCs/formulas to remove (names without extensions)"
echo ""
echo "I assume that ../generation-scripts/$target contains the VCs to filter"

target=$1
to_remove=$2


if [ "$to_remove" = "" ]
then
    echo "<to-remove> (file) not provided"
    exit 1
fi

new_target=""

case $target in
    "C-VCS-ALL")
        new_target="C-VCS-FILTERED";
        ;;
    
    "SPARK-VCS-ALL")
        new_target="SPARK-VCS-FILTERED";
        ;;

    ## Default case: bad argument -> fail
    *)
        echo "Bad argument ! <target-bench> should be in {C-VCS-ALL, SPARK-VCS-ALL} !"
        exit 1
        ;;
esac

rm -rf $new_target
cp -rf ../generation-scripts/$target $new_target ; exit_if_error ;

for f in `find $new_target -name '*'.'*'`
do
    name=`basename $f`
    abs_name=${name%.*}
    grep -c ^${abs_name}$ $to_remove 2> /dev/null > /dev/null
    if [ $? -eq 1 ]
    then
        rm $f
    fi
done
