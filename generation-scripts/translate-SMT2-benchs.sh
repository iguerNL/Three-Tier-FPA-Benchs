# For Alt-Ergo and Gappa:
#    -> Generate QF_FP unsat(+unknown) benchs from corresponding Why3 translation
#
# For Z3 and MathSAT5
#    -> Just copy SMT2 files
################################################################################

exit_if_error(){
    if [ "$?" -ne "0" ]; then exit 1; fi
}

target=$1

if [ "$target" = "" ]; then
    echo "An argument (SMT2-Griggio-Unsat+Unknown or SMT2-Wintersteiger-Unsat) is required !"
    exit 1
fi

here=`pwd`

mkdir $target ; exit_if_error

cp -rf ../sources/$target/smt2 $target/z3-440-fpa    ; exit_if_error
cp -rf ../sources/$target/smt2 $target/mathsat5-fpa  ; exit_if_error

mkdir                          $target/ae-fpa        ; exit_if_error
mkdir                          $target/ae-fpa-inline ; exit_if_error
mkdir                          $target/gappa-fpa     ; exit_if_error


why3_files=`ls ../sources/$target/why3-translation/*.why` ; exit_if_error

econf="--extra-config PATH_TO_ROOT_OF_THIS_REPO/why3-stuff/why3_extra-conf_ergos.conf"

for why3_f in $why3_files
do
    base=`basename $why3_f`
    abs_base=${base%.why}

    # ae-fpa
    why3 prove $why3_f $econf -L ../why3-stuff -P AE-OLD-FPA-LIGHT         -o $here/$target ; exit_if_error
    mv $target/*.why  $target/ae-fpa/${abs_base}.why

    # ae-fpa-inline
    why3 prove $why3_f $econf -L ../why3-stuff -P AE-OLD-FPA-INLINE-LIGHT  -o $here/$target ; exit_if_error
    mv $target/*.why  $target/ae-fpa-inline/${abs_base}.why

    # gappa-fpa
    why3 prove $why3_f $econf -L ../why3-stuff -P GAPPA-OLD-FPA-LIGHT      -o $here/$target ; exit_if_error
    mv $target/*.gappa $target/gappa-fpa/${abs_base}.gappa

done
