################################################################################

exit_if_error(){
    if [ "$?" -ne "0" ]; then exit 1; fi
}

if [ "$1" = "" ]; then
    echo "An argument (why3-of-c or vcs-of-why3) is required !"
    exit 1
fi

dirs_of_c_programs="1-Floating-point-tricks 2-Floating-point-handling 3-Numerical-analysis 4-Avionics 5-Toccata"

trans="-a split_goal_full -a split_goal_full -a split_goal_full -a split_goal_full"

here=`pwd`


## Translate C files to Why3 using "available" jessie
## Results are stored in subdirs of "sources/C-sources/"
################################################################################
translate_c_to_why3(){
    for dir in $dirs_of_c_programs
    do
        dir="../sources/C-sources/$dir"
        cd $dir
        for f in `ls *.c`
        do
            abs_name=${f%.c}
            echo ""
            echo C file = $f
            echo abs name = $abs_name
            timeout 5 frama-c -jessie -jessie-why3 prove $f > /dev/null
            exit_if_error
        done
        cd $here
    done
}

## Extract VCs for different solvers using the Why3 drivers given in why3-stuff dir
################################################################################
extract_vcs_from_why3_files(){

        mkdir C-VCS-ALL ; exit_if_error
        #mkdir C-VCS-ALL/ae-default                ; exit_if_error
        #mkdir C-VCS-ALL/ae-default-inline         ; exit_if_error
        mkdir C-VCS-ALL/ae-fpa                    ; exit_if_error
        mkdir C-VCS-ALL/ae-fpa-inline             ; exit_if_error
        mkdir C-VCS-ALL/ae-no-fpa-and-axioms      ; exit_if_error

        #mkdir C-VCS-ALL/z3-440-default            ; exit_if_error
        mkdir C-VCS-ALL/z3-440-fpa                ; exit_if_error
        mkdir C-VCS-ALL/z3-440-fpa-gnatprove      ; exit_if_error
        #mkdir C-VCS-ALL/z3-440-no-fpa-and-axioms  ; exit_if_error

        #mkdir C-VCS-ALL/cvc4-default              ; exit_if_error
        #mkdir C-VCS-ALL/cvc4-no-fpa-and-axioms    ; exit_if_error

        mkdir C-VCS-ALL/gappa-fpa                 ; exit_if_error

        mkdir C-VCS-ALL/mathsat5-fpa              ; exit_if_error

        econf=""
        econf="$econf --extra-config PATH_TO_WHY3_CONF_OF_WHY"
        econf="$econf --extra-config PATH_TO_ROOT_OF_THIS_REPO/why3-stuff/why3_extra-conf_ergos.conf"

        for dir in $dirs_of_c_programs
        do
            dir="../sources/C-sources/$dir"
            cd $dir
            for f in `ls *.c`
            do
                ff=${f%.c}
                echo ""
                echo C file = $f
                echo abs name = $ff
                
                #why3 prove ${ff}.jessie/${ff}.mlw $econf $trans -P AE-IEEE-DEFAULT           -o $here/C-VCS-ALL/ae-default
                exit_if_error

                #why3 prove ${ff}.jessie/${ff}.mlw $econf $trans -P AE-IEEE-DEFAULT-INLINE    -o $here/C-VCS-ALL/ae-default-inline
                exit_if_error

                why3 prove ${ff}.jessie/${ff}.mlw $econf $trans -P AE-IEEE-FPA               -o $here/C-VCS-ALL/ae-fpa
                exit_if_error

                why3 prove ${ff}.jessie/${ff}.mlw $econf $trans -P AE-IEEE-FPA-INLINE        -o $here/C-VCS-ALL/ae-fpa-inline
                exit_if_error

                why3 prove ${ff}.jessie/${ff}.mlw $econf $trans -P AE-IEEE-NO-FPA-AXS        -o $here/C-VCS-ALL/ae-no-fpa-and-axioms
                exit_if_error

                #why3 prove ${ff}.jessie/${ff}.mlw $econf $trans -P Z3_440-IEEE-DEFAULT       -o $here/C-VCS-ALL/z3-440-default
                #exit_if_error

                why3 prove ${ff}.jessie/${ff}.mlw $econf $trans -P Z3_440-IEEE-FPA           -o $here/C-VCS-ALL/z3-440-fpa
                exit_if_error

                #why3 prove ${ff}.jessie/${ff}.mlw $econf $trans -P Z3_440-IEEE-FPA-GNATPROVE -o $here/C-VCS-ALL/z3-440-fpa-gnatprove
                exit_if_error

                #why3 prove ${ff}.jessie/${ff}.mlw $econf $trans -P Z3_440-IEEE-NO-FPA-AXS    -o $here/C-VCS-ALL/z3-440-no-fpa-and-axioms
                exit_if_error

                #why3 prove ${ff}.jessie/${ff}.mlw $econf $trans -P CVC4-IEEE-DEFAULT         -o $here/C-VCS-ALL/cvc4-default
                exit_if_error

                #why3 prove ${ff}.jessie/${ff}.mlw $econf $trans -P CVC4-IEEE-NO-FPA-AXS      -o $here/C-VCS-ALL/cvc4-no-fpa-and-axioms
                exit_if_error

                why3 prove ${ff}.jessie/${ff}.mlw $econf $trans -P GAPPA-IEEE-FPA            -o $here/C-VCS-ALL/gappa-fpa
                exit_if_error
                
                why3 prove ${ff}.jessie/${ff}.mlw $econf $trans -P MATHSAT5-IEEE-FPA         -o $here/C-VCS-ALL/mathsat5-fpa
                exit_if_error
            done
            cd $here
        done
        
        #nb_1=`ls C-VCS-ALL/ae-default/*.why | wc -l`
        #nb_2=`ls C-VCS-ALL/ae-default-inline/*.why | wc -l`
        nb_3=`ls C-VCS-ALL/ae-fpa/*.why | wc -l`
        nb_4=`ls C-VCS-ALL/ae-fpa-inline/*.why | wc -l`
        nb_5=`ls C-VCS-ALL/ae-no-fpa-and-axioms/*.why | wc -l`
        #nb_6=`ls C-VCS-ALL/z3-440-default/*.smt2 | wc -l`
        nb_7=`ls C-VCS-ALL/z3-440-fpa/*.smt2 | wc -l`
        nb_8=`ls C-VCS-ALL/z3-440-fpa-gnatprove/*.smt2 | wc -l`
        #nb_9=`ls C-VCS-ALL/z3-440-no-fpa-and-axioms/*.smt2 | wc -l`
        #nb_10=`ls C-VCS-ALL/cvc4-default/*.smt2 | wc -l`
        #nb_11=`ls C-VCS-ALL/cvc4-no-fpa-and-axioms/*.smt2 | wc -l`
        nb_12=`ls C-VCS-ALL/gappa-fpa/*.gappa | wc -l`
        nb_13=`ls C-VCS-ALL/mathsat5-fpa/*.smt2 | wc -l`

        echo ""
        #echo "C-VCS-ALL/ae-default               : $nb_1"
        #echo "C-VCS-ALL/ae-default-inline        : $nb_2"
        echo "C-VCS-ALL/ae-fpa                   : $nb_3"
        echo "C-VCS-ALL/ae-fpa-inline            : $nb_4"
        echo "C-VCS-ALL/ae-no-fpa-and-axioms     : $nb_5"
        #echo "C-VCS-ALL/z3-440-default           : $nb_6"
        echo "C-VCS-ALL/z3-440-fpa               : $nb_7"
        echo "C-VCS-ALL/z3-440-fpa-gnatprove     : $nb_8"
        #echo "C-VCS-ALL/z3-440-no-fpa-and-axioms : $nb_9"
        #echo "C-VCS-ALL/cvc4-default             : $nb_10"
        #echo "C-VCS-ALL/cvc4-no-fpa-and-axioms   : $nb_11"
        echo "C-VCS-ALL/gappa-fpa                : $nb_12"
        echo "C-VCS-ALL/mathsat5-fpa             : $nb_13"
        cd $here
}


## Sanitize MathSAT5 BENCHS
################################################################################
no_bool_args_for_mathsat5(){
    # remove useless, but insupported mathsat5 functions
    cd C-VCS-ALL/mathsat5-fpa
    for f in `ls *.smt2`
    do
        #echo $f
        rm -rf /tmp/temp.mathsat5-fpa
        grep -v "(declare-fun match_bool (ty Bool uni uni) uni)" $f | grep -v "(declare-fun index_bool (Bool) Int)" > /tmp/temp.mathsat5-fpa
        mv /tmp/temp.mathsat5-fpa $f
    done
    cd $here
}

# Main entry
################################################################################
case $1 in
    "why3-of-c")
        translate_c_to_why3;
        exit 0
        ;;
    "vcs-of-why3")
        extract_vcs_from_why3_files;
        no_bool_args_for_mathsat5;
        exit 0
        ;;

    ## Default case: bad argument -> fail
    *)
        echo "Bad argument ! (why3-of-c or vcs-of-why3) is required !"
        exit 1
        ;;
esac

