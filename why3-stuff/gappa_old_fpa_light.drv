
(* Why driver for Gappa *)

prelude "# this is a prelude for Gappa"

printer "gappa"
filename "%f-%t-%g.gappa"

valid 0
unknown "no contradiction was found\\|some enclosures were not satisfied\\|some properties were not satisfied" ""
time "why3cpulimit time : %s s"
fail "Error: \\(.*\\)" "\\1"

transformation "inline_trivial"
transformation "eliminate_builtin"
transformation "inline_all"
transformation "eliminate_definition"
transformation "eliminate_inductive"
transformation "eliminate_algebraic"
transformation "eliminate_epsilon"
transformation "eliminate_if"
transformation "eliminate_let"
transformation "simplify_formula"
transformation "simplify_unknown_lsymbols"
transformation "simplify_trivial_quantification"
transformation "introduce_premises"
transformation "instantiate_predicate"
transformation "abstract_unknown_lsymbols"

theory BuiltIn
  syntax type int   "int"
  syntax type real  "real"
  syntax predicate (=)  "dummy"
  meta "eliminate_algebraic" "keep_enums"
end

theory int.Int

  prelude "# this is a prelude for Gappa integer arithmetic"

  syntax function zero "0"
  syntax function one  "1"

  syntax function (+)  "(%1 + %2)"
  syntax function (-)  "(%1 - %2)"
  syntax function ( * )  "(%1 * %2)"
  syntax function (-_) "(-%1)"

  syntax predicate (<=) "dummy"
  syntax predicate (>=) "dummy"
  syntax predicate (<)  "dummy"
  syntax predicate (>)  "dummy"

  meta "gappa arith" predicate (<=), "", "<=", ">="
  meta "gappa arith" predicate (>=), "", ">=", "<="
  meta "gappa arith" predicate (<), "not ", ">=", "<="
  meta "gappa arith" predicate (>), "not ", "<=", ">="

  meta "inline : no" predicate (<=)
  meta "inline : no" predicate (>=)
  meta "inline : no" predicate (>)

  remove prop NonTrivialRing
  remove prop ZeroLessOne

end

theory int.Abs

  syntax function abs  "| %1 |"

end

theory int.ComputerDivision

  syntax function div "int<zr>(%1 / %2)"
  syntax function mod "(%1 - int<zr>(%1 / %2) * %2)"

end

theory real.Real

  prelude "# this is a prelude for Gappa real arithmetic"

  syntax function zero "0.0"
  syntax function one  "1.0"

  syntax function (+)  "(%1 + %2)"
  syntax function (-)  "(%1 - %2)"
  syntax function ( * )  "(%1 * %2)"
  syntax function (/)  "(%1 / %2)"
  syntax function (-_) "(-%1)"
  syntax function inv  "(1.0 / %1)"

  syntax predicate (<=) "dummy"
  syntax predicate (>=) "dummy"
  syntax predicate (<)  "dummy"
  syntax predicate (>)  "dummy"

  meta "gappa arith" predicate (<=), "", "<=", ">="
  meta "gappa arith" predicate (>=), "", ">=", "<="
  meta "gappa arith" predicate (<), "not ", ">=", "<="
  meta "gappa arith" predicate (>), "not ", "<=", ">="

  meta "inline : no" predicate (<=)
  meta "inline : no" predicate (>=)
  meta "inline : no" predicate (>)

  remove prop NonTrivialRing
  remove prop ZeroLessOne

end

theory real.Abs

  syntax function abs  "| %1 |"

end

theory real.Square

  syntax function sqrt  "sqrt(%1)"

end

theory real.Truncate

  syntax function truncate "int<zr>(%1)"
  syntax function floor    "int<dn>(%1)"
  syntax function ceil     "int<up>(%1)"

end

theory real.FromInt

  syntax function from_int "%1"

  remove prop Zero
  remove prop One

end

theory floating_point_light.Rounding

  syntax function NearestTiesToEven "ne"
  syntax function ToZero "zr"
  syntax function Up "up"
  syntax function Down "dn"
  syntax function NearestTiesToAway "na"

end

theory floating_point_light.GenFloat

  syntax function integer_round "fixed<0,%1>(%2)"

end

theory floating_point_light.Single

  syntax function round "float<ieee_32,%1>(%2)"
  meta "instantiate : auto" prop Bounded_value

end

theory floating_point_light.Double

  syntax function round "float<ieee_64,%1>(%2)"
  meta "instantiate : auto" prop Bounded_value

end

theory floating_point_light.SingleFull

  syntax function round "float<ieee_32,%1>(%2)"
  meta "instantiate : auto" prop Bounded_value

end

theory floating_point_light.DoubleFull

  syntax function round "float<ieee_64,%1>(%2)"
  meta "instantiate : auto" prop Bounded_value

end

(*
theory floating_point_light.SingleMultiRounding

  syntax function round "float<ieee_32,%1>(%2)"
  meta "instantiate : auto" prop Bounded_value

end
*)

theory floating_point_light.DoubleMultiRounding

  syntax function round "float<ieee_64,%1>(%2)"
  meta "instantiate : auto" prop Bounded_value

end


(*
Local Variables:
mode: why
compile-command: "make -C .. bench"
End:
*)
