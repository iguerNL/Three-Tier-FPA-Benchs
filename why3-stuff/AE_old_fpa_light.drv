import "alt_ergo.drv"

import "new_builtin_reals_ints.drv"

theory floating_point_light.Rounding
  syntax type mode "fpa_rounding_mode"
  syntax function NearestTiesToEven "NearestTiesToEven"
  syntax function ToZero "ToZero"
  syntax function Up "Up"
  syntax function Down "Down"
  syntax function NearestTiesToAway "NearestTiesToAway" 
  (** nearest ties to even, to zero, upward, downward, nearest ties to away *)
end


theory floating_point_light.Double
   syntax function round "float64(%1, %2)"
   syntax function integer_round "real_of_int(integer_round(%1, %2))"
end

theory floating_point_light.Single
   syntax function round "float32(%1, %2)"
   syntax function integer_round "real_of_int(integer_round(%1, %2))"
end

theory floating_point_light.DoubleFull
   syntax function round "float64(%1, %2)"
   syntax function integer_round "real_of_int(integer_round(%1, %2))"
end

theory floating_point_light.SingleFull
   syntax function round "float32(%1, %2)"
   syntax function integer_round "real_of_int(integer_round(%1, %2))"
end

theory floating_point_light.DoubleMultiRounding
   syntax function round "float64(%1, %2)"
   syntax function integer_round "real_of_int(integer_round(%1, %2))"
end

theory floating_point_light.GenFloat
  remove prop Round_monotonic
  remove prop Round_idempotent
end
