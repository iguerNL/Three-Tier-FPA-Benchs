import "alt_ergo.drv"


theory floating_point.Rounding
 (* no axioms except those encoding sum type *)
end


theory floating_point.SpecialValues
  remove prop same_sign_real_zero1
  remove prop same_sign_real_zero2
  remove prop same_sign_real_zero3
  remove prop same_sign_real_correct2
  remove prop same_sign_real_correct3

end


theory floating_point.GenFloat
  prelude "logic round_error : 'a -> real"
  prelude "logic total_error : 'a -> real"
  prelude "logic no_overflow  : 'a, real -> prop"

  syntax function round_error "round_error(%1)"
  syntax function total_error "total_error(%1)"
  syntax predicate no_overflow "no_overflow(%1,%2)"

  remove prop Bounded_real_no_overflow
  remove prop Round_monotonic
  remove prop Round_idempotent
  remove prop Round_value
  remove prop Bounded_value
  remove prop Exact_rounding_for_integers
  remove prop Round_down_le
  remove prop Round_up_ge
  remove prop Round_down_neg
  remove prop Round_up_neg
  remove prop Round_logic_def
end


theory floating_point.SingleFormat
 (* no axioms except those encoding sum type *)
end


theory floating_point.DoubleFormat
 (* no axioms except those encoding sum type *)
end


theory floating_point.GenFloatSpecStrict
   prelude "logic of_real_post : 'a, 'b, 'c -> prop"
   prelude "logic add_post : 'a, 'b, 'c, 'd -> prop"
   prelude "logic sub_post : 'a, 'b, 'c, 'd -> prop"
   prelude "logic mul_post : 'a, 'b, 'c, 'd -> prop"
   prelude "logic div_post : 'a, 'b, 'c, 'd -> prop"
   prelude "logic neg_post : 'a, 'b -> prop"
   prelude "logic lt : 'a, 'b -> prop"
   prelude "logic gt : 'a, 'b -> prop"
   syntax predicate of_real_post "of_real_post(%1,%2,%3)"
   syntax predicate add_post "add_post(%1,%2,%3,%4)"
   syntax predicate sub_post "sub_post(%1,%2,%3,%4)"
   syntax predicate mul_post "mul_post(%1,%2,%3,%4)"
   syntax predicate div_post "div_post(%1,%2,%3,%4)"
   syntax predicate neg_post "neg_post(%1,%2)"
   syntax predicate lt "lt(%1,%2)"
   syntax predicate gt "gt(%1,%2)"
end


theory floating_point.Single
  (* no axioms or lemmas *)
end


theory floating_point.Double
  (* no axioms or lemmas *)
end


theory floating_point.GenFloatFull

  prelude "logic  is_finite : 'a -> prop"
  prelude "logic  is_infinite : 'a -> prop"
  prelude "logic  is_NaN : 'a -> prop"
  prelude "logic  is_not_NaN : 'a -> prop"
  prelude "logic  same_sign_real___bis : 'a, real -> prop"
  prelude "logic  same_sign : 'a, 'b -> prop"
  prelude "logic  diff_sign : 'a, 'b -> prop"
  prelude "logic  sign_zero_result : 'a, 'b -> prop"
  prelude "logic  is_minus_infinity : 'a -> prop"
  prelude "logic  is_plus_infinity : 'a -> prop"
  prelude "logic  is_gen_zero : 'a -> prop"
  prelude "logic  is_gen_zero_plus : 'a -> prop"
  prelude "logic  is_gen_zero_minus : 'a -> prop"
  prelude "logic  overflow_value : 'a, 'b -> prop"

  syntax predicate is_finite "is_finite(%1)"
  syntax predicate is_infinite "is_infinite(%1)"
  syntax predicate is_NaN "is_NaN(%1)"
  syntax predicate is_not_NaN "is_not_NaN(%1)"
  syntax predicate same_sign_real "same_sign_real___bis(%1,%2)"
  syntax predicate same_sign "same_sign(%1,%2)"
  syntax predicate diff_sign "diff_sign(%1,%2)"
  syntax predicate sign_zero_result "sign_zero_result(%1,%2)"
  syntax predicate overflow_value "overflow_value(%1,%2)"
  syntax predicate is_minus_infinity "is_minus_infinity(%1)"
  syntax predicate is_plus_infinity "is_plus_infinity(%1)"
  syntax predicate is_gen_zero "is_gen_zero(%1)"
  syntax predicate is_gen_zero_plus "is_gen_zero_plus(%1)"
  syntax predicate is_gen_zero_minus "is_gen_zero_minus(%1)"

  remove prop is_not_NaN
  remove prop finite_sign
  remove prop finite_sign_pos1
  remove prop finite_sign_pos2
  remove prop finite_sign_neg1
  remove prop finite_sign_neg2
  remove prop diff_sign_trans
  remove prop diff_sign_product
  remove prop same_sign_product
  remove prop round1
  remove prop round2
  remove prop round3
  remove prop round4
  remove prop round_of_zero
  remove prop round_logic_le
  remove prop round_no_overflow
  remove prop positive_constant
  remove prop negative_constant
  remove prop is_gen_zero_comp1
  remove prop is_gen_zero_comp2
end


theory floating_point.GenFloatSpecFull

  prelude "logic add_post____2 : 'a, 'b, 'c, 'd -> prop"
  prelude "logic sub_post____2 : 'a, 'b, 'c, 'd -> prop"
  prelude "logic product_sign____2 : 'a, 'b, 'c -> prop"
  prelude "logic mul_post____2 : 'a, 'b, 'c, 'd -> prop"
  prelude "logic neg_post____2 : 'a, 'b -> prop"
  prelude "logic div_post____2 : 'a, 'b, 'c, 'd -> prop"
  prelude "logic fma_post____2 : 'a, 'b, 'c, 'd, 'e -> prop"
  prelude "logic sqrt_post____2 : 'a, 'b, 'c -> prop"
  prelude "logic of_real_exact_post____2 : 'a, 'b -> prop"
  prelude "logic le____2 : 'a, 'b -> prop"
  prelude "logic lt____2 : 'a, 'b -> prop"
  prelude "logic ge____2 : 'a, 'b -> prop"
  prelude "logic gt____2 : 'a, 'b -> prop"
  prelude "logic eq____2 : 'a, 'b -> prop"
  prelude "logic ne____2 : 'a, 'b -> prop"

  syntax predicate add_post "add_post____2(%1,%2,%3,%4)"
  syntax predicate sub_post "sub_post____2(%1,%2,%3,%4)"
  syntax predicate product_sign "product_sign____2(%1,%2,%3)"
  syntax predicate mul_post "mul_post____2(%1,%2,%3,%4)"
  syntax predicate neg_post "neg_post____2(%1,%2)"
  syntax predicate div_post "div_post____2(%1,%2,%3,%4)"
  syntax predicate fma_post "fma_post____2(%1,%2,%3,%4,%5)"
  syntax predicate sqrt_post "sqrt_post____2(%1,%2,%3)"
  syntax predicate of_real_exact_post "of_real_exact_post____2(%1,%2)"
  syntax predicate le "le____2(%1,%2)"
  syntax predicate lt "lt____2(%1,%2)"
  syntax predicate ge "ge____2(%1,%2)"
  syntax predicate gt "gt____2(%1,%2)"
  syntax predicate eq "eq____2(%1,%2)"
  syntax predicate ne "ne____2(%1,%2)"

  remove prop le_lt_trans
  remove prop lt_le_trans
  remove prop le_ge_asym
  remove prop not_lt_ge
  remove prop not_gt_le

end


theory floating_point.SingleFull
  (* no axioms or lemmas *)
end


theory floating_point.DoubleFull
  (* no axioms or lemmas *)
end


theory floating_point.GenFloatSpecMultiRounding

  prelude "logic add_post____3 : 'a, 'b, 'c, 'd -> prop"
  prelude "logic sub_post____3 : 'a, 'b, 'c, 'd -> prop"
  prelude "logic mul_post____3 : 'a, 'b, 'c, 'd -> prop"
  prelude "logic neg_post____3 : 'a, 'b  -> prop"
  prelude "logic of_real_post____3 : 'a, real, 'c -> prop"
  prelude "logic of_real_exact_post____3 : real, 'a -> prop"
  prelude "logic lt____3 : 'a, 'b -> prop"
  prelude "logic gt____3 : 'a, 'b -> prop"

  syntax predicate add_post "add_post____3(%1,%2,%3,%4)"
  syntax predicate sub_post "sub_post____3(%1,%2,%3,%4)"
  syntax predicate mul_post "mul_post____3(%1,%2,%3,%4)"
  syntax predicate neg_post "neg_post____3(%1,%2)"
  syntax predicate of_real_post "of_real_post____3(%1,%2,%3)"
  syntax predicate of_real_exact_post "of_real_exact_post____3(%1,%2)"
  syntax predicate lt "lt____3(%1,%2)"
  syntax predicate gt "gt____3(%1,%2)"
end


theory floating_point.DoubleMultiRounding
  (* no axioms or lemmas *)
end

(* TODO: other theories from floating_point ? from jessie model ? *)
