**This work is supported by the ANR projects SOPRANO (ANR-14-CE28-0020)
and ProofInUse (ANR-14-LAB3-0007)**

<div style="text-align:center"><img src ="misc/ocamlpro-logo.png" /></div>

# 1. Content of this Repository

This repository contains the material used to evaluate Alt-Ergo's
Floating-Point reasoning approach described in the CAV'17 paper: [A
Three-tier Strategy for Reasoning about Floating-Point Numbers in
SMT](misc/paper-submitted-CAV-2017--Authors-version.pdf). In particular, it contains:

- in `sources`: the original C and SPARK programs, the original SMT2
benchmarks and their why3 translation from which the VCs have been
generated,
- in `solvers`: the different solvers that we used for the evaluation,
- in `final-filtered-VCs`: the formulas (VCs) that have been sent to
  the solvers,
- in `translators`: the Why and Why3 versions that have been used during the
  generation process
- in `why3-stuff`: why3 drivers that have been used to generate
  the VCs in different formats, and a why3 extra-config file that
  references these drivers
- in `generation-scripts`: the scripts that allow to re-generate the
  benchmarks
- in `filtering-scripts`: a script that allows to filter newly
  re-generated benchmarks (only applies for C and SPARK benchmarks)


**If you are only interested in the final filtered VCs, and
   possibly in the solvers used for the evaluation,** you can
   consider the content of the directories `final-filtered-VCs` and
   `solvers` and ignore the rest of the repository.

# 2. Re-generating the VCs from the sources

## 2.a. Common steps

The first step is to setup a working environment. The best solution is
to use the
[OPAM package manager](http://opam.ocaml.org/doc/Install.html) . The
following process has been tested with OPAM 1.2.2 on a fresh
Xubuntu 17.04 (inside VirtualBox).

Once OPAM is installed, create an OCaml 4.03.0 switch, install some
system dependencies and particular versions of Frama-c, Why and Why3:

```
$ opam sw 4.03.0   # or $ opam init --comp=4.03.0 if opam was not initialized
$ eval `opam config env`
$ opam depext conf-gmp.1 conf-gnomecanvas.2 conf-gtksourceview.2 conf-m4.1
$ opam install frama-c.20161101 why.2.38 why3.0.87.3
```

Actually, we need some dev versions of Why and Why3 (provided in
`translators` directory). To install them, proceed as follows for
Why3:

```
$ cd translators
$ tar -xf why3--2016-12-07--91e338e01dda86a1c9371b252438979c7c2efb8b.tar.bz2
$ cd why3--2016-12-07--91e338e01dda86a1c9371b252438979c7c2efb8b
$ autoconf
$ ./configure --prefix=<PATH-TO-DOT-OPAM-DIR>/4.03.0/
$ make
$ make install
$ why3 config --detect-provers
$ cd ..
```

and as follows for Why:

```
$ tar -xf why--2016-10-11--d960cc7508c71070163fd10fed7133db4e411720.tar.bz2
$ cd why--2016-10-11--d960cc7508c71070163fd10fed7133db4e411720
$ autoconf
$ ./configure --prefix=<PATH-TO-DOT-OPAM-DIR>/4.03.0/
$ make
$ make install
```

Usually, `--prefix=<PATH-TO-DOT-OPAM-DIR>/4.03.0/` is just
`--prefix=$HOME/.opam/4.03.0/`.

Now, edit `global-configuration.sh` and complete/fix
the paths it contains if needed. Then execute it.

**Note that, we temporarily provide [a virtual machine image with a working environment](https://www.lri.fr/~iguer/Xubuntu-17.04--eval-FPA-CAV-2017.ova) (its sha1sum is 5e08ba9a7bf26c77399ddb0410aec8e599f01492)**

## 2.b. Generating VCs from C programs

Once a working environment is correctly configured, do the following to extract the (unfiltered) VCs from the C programs. The result will be stored in 
`generation-scripts/C-VCS-ALL`.

```
$ cd sources
$ tar -xf C-sources.tar.bz2
$ cd ../generation-scripts/
$ ./VCs-of-C-programs.sh why3-of-c
$ ./VCs-of-C-programs.sh vcs-of-why3
```

Note that the latest step will take some time to generate all the VCs
in different formats.

## 2.c. Generating VCs from SPARK programs

To generate the (unfiltered) VCs, proceed as follows:


```
$ cd sources
$ tar -xf SPARK-sources.tar.bz2
$ cd ../generation-scripts/
$ ./VCs-of-SPARK-programs.sh
```

The result of the latest step will be stored in
`generation-scripts/SPARK-VCS-ALL`. Note that this step will take a
while (more than 5 hours on my machine) to generate all the VCs in
different formats.

## 2.d. Translating SMT2 benchmarks to Alt-Ergo and Gappa

The (filtered) SMT2 benchmarks and a Why3 translation are already
provided in `sources/SMT2-Griggio-Unsat+Unknown.tar.bz2` and in
`sources/SMT2-Wintersteiger-Unsat.tar.bz2`. Contrary to Alt-Ergo and
Gappa, we use the original SMT2 files for solvers that support SMT2
input format. Here is how to get the formulas in all solvers' input formats:

```
$ cd sources
$ tar -xf SMT2-Griggio-Unsat+Unknown.tar.bz2
$ tar -xf SMT2-Wintersteiger-Unsat.tar.bz2
$ cd ../generation-scripts/
$ ./translate-SMT2-benchs.sh SMT2-Griggio-Unsat+Unknown
$ ./translate-SMT2-benchs.sh SMT2-Wintersteiger-Unsat
```

The two latest steps will translate Griggio and Wintersteiger filtered
benchmarks, and store the results in
`generation-scripts/SMT2-Griggio-Unsat+Unknown` and
`generation-scripts/SMT2-Wintersteiger-Unsat`, respectively.

**Update:** Our small OCaml program that translates FP SMT2 files to
Why3 is now provided in `translators/fp-smt2-to-why3`. Note that:
- it may fail to translate other FP benchmarks that are not provided
in this repo, as it was built to only fit our need
- we know (recently realized) that the translation to Alt-Ergo and Gappa (via
  Why3) is not optimal (but this does not affect other solvers that
  support SMT2)
- see section **4.** for additional remarks.

The translation of the SMT2 example provided in
`translators/fp-smt2-to-why3` can be done with the following command:

```
./fp-smt2-to-why3 -input example.smt2 -output example_translated_to_why3_format.why
```

The resulting Why3 file can then be translated to Alt-Ergo using, for
instance, the command:

```
why3 prove --extra-config ../../why3-stuff/why3_extra-conf_ergos.conf example_translated_to_why3_format.why -L ../../why3-stuff -P AE-OLD-FPA-LIGHT -o .
```
that will generate an Alt-Ergo file `example_translated_to_why3_format-EXAMPLE-g_1.why`.

# 3. Filtering the VCs

As explained in the CAV paper, we applied a filtering step to remove
from C and SPARK benchmarks all the VCs that do not need
Floating-Point reasoning to be proved. For that, we discard any VC
that is proved by Alt-Ergo without any built-in or axiomatized FP
reasoning. This filtering process is clearly a fair for other solvers
but possibly not for Alt-Ergo. In fact, it may fail to prove a VC
(and thus discard it) because of a weakness in non-linear rational
arithmetic, for instance.

# 4. TODO list
- try the `fast-wp` VCs generator of why3,
- port the smt2-to-why3 translator to the new Why3 FPA theory, improve
  the translation of SMT2 files to Why3, and translate Other QF_FP
  benchmarks of SMT-LIB (schanda, ramalho,
  20170501-Heizmann-UltimateAutomizer),
- improve why3 drivers and filtering process,


# 5. Some encountered issues
- Issues with MathSAT5: `Functions with Bool arguments not supported`,
  `unknown symbol: fp.to_real`, `division between two non-constant
  arguments unsupported`

# 6. Additional clarifications

- For SPARK benchmarks, we generated z3 VCs using two distinct Why3
  drivers: `z3_gnatprove.drv` and `z3_440_ieee_FPA.drv`. We used the
  bench we got with the second driver for the paper, as the success
  rate of z3 was better on it.

- Contrary to what we said in the paper, `schanda` benchmarks (of
  SMT-LIB) were apparently not included in SPARK test-suite.
